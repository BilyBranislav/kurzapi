﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace KurzAPI.Services
{

    [ApiController]
    [Route("api/news")]

    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;
        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpGet]
        [Route("latest-headline")]
        public string LatestHeadLine()
        {
           return _newsService.LatestHeadLine();
        }
    }
}
