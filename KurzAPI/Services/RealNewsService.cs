﻿using System;
using Microsoft.Extensions.Logging;

namespace KurzAPI.Services
{
    public class RealNewsService : INewsService
    {

        private readonly DateTime _now;
        private readonly ILogger<RealNewsService> _logger;

        public RealNewsService(ILogger<RealNewsService> logger)
        {
            _now = DateTime.Now;
            _logger = logger;
        }

        public string LatestHeadLine()
        {
            _logger.LogInformation("Called LatestHeadLine");
            return $"Viktoria sa stve ked nahlas programujem :( o {_now:T}";
        }
    }
}
