﻿using System;
namespace KurzAPI.Services
{
    public interface INewsService
    {
        string LatestHeadLine();
    }
}
