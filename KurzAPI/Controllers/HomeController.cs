﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace KurzAPI.Controllers
{
    [ApiController]
    [Route("api/v2/[controller]")]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        [Route("helloworld")]
        public string SayHello() => $"Hello World!, at {DateTime.Now:g}";

        [HttpGet]
        [Route("greet")]
        public string Greet([FromQuery] string name) => $"Hello {name}";

        [HttpGet]
        [Route("greet/{name}")]
        public string GreetName( string name) => $"Hello {name}";

        [HttpGet]
        [Route("user")]
        public string GetUserByName([FromQuery] string user) => $"Hello {user}";

        [HttpGet]
        [Route("user/{userId:int}")]
        public string GetUserById(int userID) => $"Hello {userID}";

    }
}